import { IPublisher, ISubscriber, ArrowKeyCode, KeyEvent } from "./types";

export default class EventHandler implements IPublisher {
  private static instance: EventHandler;
  subscribers: Map<KeyEvent, ISubscriber[]> = new Map();

  private constructor() {
    document.addEventListener('keyup', event => this.fireSubscription(event))
  }

  static getInstance() {
    if (!EventHandler.instance) {
      EventHandler.instance = new EventHandler();
    }
    return EventHandler.instance;
  }

  addListener() {}

  fireSubscription(e: KeyboardEvent) {
    let key_ev: KeyEvent | null = null;
    switch (e.code) {
      case ArrowKeyCode.Left:
        key_ev = KeyEvent.Left;
        break;
      case ArrowKeyCode.Up:
        key_ev = KeyEvent.Up;
        break;
      case ArrowKeyCode.Right:
        key_ev = KeyEvent.Right;
        break;
      case ArrowKeyCode.Down:
        key_ev = KeyEvent.Down;
        break;
      default:
        break;
    }
    if (key_ev) {
      const subscribers_by_event = this.subscribers.get(key_ev);
      subscribers_by_event?.forEach((subscriber: ISubscriber) =>
        subscriber.callback(e)
      );
    }
  }

  subscribe(subscriber: any, key_event: KeyEvent, callback: Function) {
    let subscribers_by_event = this.subscribers.get(key_event);
    if (!subscribers_by_event) {
      subscribers_by_event = [];
      this.subscribers.set(key_event, subscribers_by_event);
    }
    if (!subscribers_by_event?.includes(subscriber)) {
      subscribers_by_event.push({
        subscriber,
        callback
      });
    }
  }

  unsubscribe(subscriber: any, key_event: KeyEvent) {
    const subscribers_by_event = this.subscribers.get(key_event);
    if (subscribers_by_event) {
      const index = subscribers_by_event.findIndex(
        (sub: ISubscriber) => sub.subscriber === subscriber
      );
      if (index !== -1) {
        subscribers_by_event.splice(index, 1);
      }
    }
  }
}
