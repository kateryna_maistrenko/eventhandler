import {IEventHandler} from "@/types";

export default function Inject(EventHandler: IEventHandler) {
  return function(target: any, propertyKey: string) {
    target[propertyKey] = EventHandler.getInstance();
  };
}
