import EventHandler from "@/eventHandler";

export interface ISubscriber {
  // update(subject: Publisher): void
  subscriber: any;
  callback: Function;
}

export interface IPublisher {
  subscribers: Map<KeyEvent, any[]>;

  subscribe(
    subscriber: ISubscriber,
    key_event: KeyEvent,
    callback: Function
  ): void;

  unsubscribe(subscriber: ISubscriber, key_event: KeyEvent): void;
  fireSubscription(e: Event): void;
}

export enum ArrowKeyCode {
  Left = "ArrowLeft",
  Up = "ArrowUp",
  Right = "ArrowRight",
  Down = "ArrowDown"
}

export enum KeyEvent {
  Left = "left",
  Up = "up",
  Right = "right",
  Down = "down"
}

export interface IEventHandler extends Function {
  getInstance: () => EventHandler
}
